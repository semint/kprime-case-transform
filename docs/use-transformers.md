# Use Transformers

### if not already connected, connect the context

On home page:

* Login from browser with credentials

    gino
    pass

* Click on Context button "Manage"

On Contexts page:

* Insert into text field and then press Enter:

    /transform/

* Click on "Select" button

Now on home page:

there should now appear 3 cards: Goals, Stories, Terms.

### have a look at the terms

* From home page click on button "Manage" of card Terms.

Have a look at current terms.

### check transformers applicability

* Insert this command and press Enter:

    >trans-applicability 

you should see :


    Command Result:

    vertical COMPOSE composeApplicable false violation: originTable2:/database/schema/constraints/constraints[@type='DOUBLE_INCLUSION'][1]/target/@table > 0 [] size <= 0
    originTable1 []
    originTable2 []
    lhs []
    rhs []
    originTable []
    DECOMPOSE decomposeApplicable false violation: rhss:/database/schema/constraints/constraints[@type='FUNCTIONAL'][1]/target[@table='originTable']/columns/columns/@name > 0 [] size <= 0
    originTable []
    all []
    keys []
    lhss []
    rhss []
    rests []
    table []
    view1 []
    view2 []

    horizontal COMPOSE Empty rules.
    DECOMPOSE required [originTable, condition, not_condition] parameter(s).



### apply transformer

There are already two transformers, so you should see a "Transformations" panel.

* Click on "applicability" button

Should appear applicability conditions with a "vertical transform" option

* Click on "vertical transformer" to apply option.

Terms should be changed.
You can look at the older versions 

* Old terms, click on "Previous" button near "base" on Terms panel.

Have a look at current terms.
If you want to go back to current terms version.

* Current terms, click on "Next" button near "base" on Terms panel.

### apply horizontal transformer

Should appear applicability conditions with a "horizontal transform" option.

* Click on "horizontal transformer" to apply option.

There are required parameters, 
there is a link to "horizontal transform story"

* Click on "horizontal transform story" to open the story
