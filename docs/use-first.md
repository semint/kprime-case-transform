# First Use

### connect the context

On home page:

* Login from browser with credentials

    gino
    pass

* Click on Context button "Manage"

On Contexts page:

* Insert into text field and then press Enter:

    /transform/

* Click on "Select" button

Now on home page:

there should now appear 3 cards: Goals, Stories, Terms.

### have a look at the goals

On home page:

* Click on button "Manage" of card Goals

Have a look at the "transform" context goals.

### have a look at the stories

* From home page click on button "Manage" of card Stories.

or

* From goals page click the third meun bar button for fast switch to Stories page.

* Choose "traces" from menu button "Folders".

* Choose "root" from menu button "Folders".

* Choose "to_arm.md" from menu button "Stories".

A text of commands whould appear. Have a look at it.

### play some commands 

To execute all the commands inside one story 

* Click on button "Play" in toolbar.

### have a look at the terms

* From home page click on button "Manage" of card Terms.

or

* From goals page click the forth menu bar button for fast switch to Terms page.

Have a look at current terms.
If you have played the "to_arm.md" story, now there are more versions of terms.
You can look at the older versions 

* Old terms, click on "Previous" button near "base" on Terms panel.

Have a look at current terms.
If you want to go back to current terms version.

* Current terms, click on "Next" button near "base" on Terms panel.

### disconnect from kprime

On home page:

* Click on button "Exit"

WARNING Kprime server is still running. To use it again just go to login page.



## stop

To stop servers from another shell with:

    make stop

or with:

    docker-compose down

