# README

## Requirements

* docker (tested with Docker version 20.10.7, build 20.10.7-0ubuntu5~20.04.2)
* docker-compose (tested with docker-compose version 1.29.2, build 5becea4c)

## Run

start servers from shell with:

    make run

or

    docker-compose up

## Try KPrime ping

try KPrime from a browser with:

    http://localhost:7007/login

you should see the login page.

## Try SpinOne ping

Try SpinOne from a browser with:

    http://localhost:7997/

output:

    {
    "msg": {
        "uuid": "0",
        "label": "ok",
        "level": "info",
        "description": "Hello from KPrime Expert SpinOne 0.3, available commands:\n    vsplit - try apply a vertical split, as success result adds a new database to current trace and termabase.\n    vjoin - try apply a vertical join, as success result adds a new database to current trace and termabase."
    },
    "payload": null,
    "options": {
        "options": [
        {
            "optLabel": "VSplit",
            "optComand": "vsplit"
        },
        {
            "optLabel": "VJoin",
            "optComand": "vjoin"
        }
        ]
    }
    }

## Use 

Follow if you like this usage tutorials.

NOTE: Any change you will do will be persisted in current position (.) of the file system where kprime is started.


### Tutorials

[First use tutorial](docs/use-first.md) - move your first steps

[Transformers tutorial](docs/use-transformers.md) - leran how to use internal transformers

[SpinOne Expert tutorial](docs/use-spinone.md) - learn how to use external experts