# Story story_horizontal

## Horizontal Decompose 

add-trace horizontal

current-trace horizontal

add-table person:name,address,dob,spouse

set-db-name horizontal1

save-database

required [originTable, condition] parameter(s).

add-param originTable person

add-param condition spouse is not null

---
trans-applicability 

<pre>
horizontal COMPOSE Empty rules.
 DECOMPOSE decomposeApplicable true 
 all []
 originTable [person]
 condition [spouce is not null]
</pre>

---

trans-apply horizontal decompose

