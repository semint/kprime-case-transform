# Story demo2_1

## Given

[Table0 SQL](Story_demo2-sql.md#2)

[Table0 META](Story_demo2-meta.md#2)
---
## When demo 2.1

### vertical decomposition MVD SSN->>Phone
#### trigger MVD.1:
<pre>
LHS=SSN, 
RHS=Phone, 
REST=Name,DepName,DepAdd, 
CONSTR=FD1,FD2,DOUBLE_INC 
RHS'=_
</pre>

---
#### changeset

>set-source demo2

>set-db-name demo2_1

>changeset-clean

>add-cs-table ssn_phone:SSN,Phone

>add-cs-table ssn_others:SSN,Name,DepName,DepAddress

>add-cs-double-inc ssn_phone:SSN<->ssn_others:SSN

>del-cs-table table0

add-cs-mapping ssn_phone select SSN,Phone from table0

add-cs-mapping ssn_others select SSN,Name,DepName,DepAddress from table0

>changeset-apply

changeset-exec-mappings

>trace-database demo2_1

---
## Then

#### database

>all

[view DIAGRAM](/chart/classes/demo2/demo2_1_tracedb.xml)

>select * from ssn_phone

[view ssn_phone JSON](/data/demo2/demo2_1_tracedb.xml/ssn_phone)

>select * from ssn_others

[view ssn_others JSON](/data/demo2/demo2_1_tracedb.xml/ssn_others)

---
#### mappings
<pre>
ssn_phone : select SSN,Phone from table0
ssn_others : select SSN,Name,DepName,DepAdd from table0
</pre>

#### reverse mappings
<pre>
table0 : select * from ssn_phone 
    join ssn_others on ssn_phone.SSN=ssn_others.SSN
</pre>