# Story demo1-sql

## setup SQL table0

>new-source-h2 demo2

>set-source demo2

>drop all objects

>create table table0(SSN varchar(64), Phone varchar(64), Name varchar(64), DepName varchar(64), DepAddress varchar(64))

>insert into table0 values('SSN1', 'Phone1', 'Name1', 'DepName1', 'DepAddress1')

>insert into table0 values('SSN2', 'Phone2', 'Name2', 'DepName2', 'DepAddress2')

>insert into table0 values('SSN3', 'Phone3', 'Name3', 'DepName2', 'DepAddress2')

>insert into table0 values('SSN4', 'Phone4', 'Name4', NULL, NULL)

>select * from table0

