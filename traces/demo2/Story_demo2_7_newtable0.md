# Story demo2_7

## Given

<pre>
Tables:
</pre>
---
## When

### delete table5
#### changeset
<pre>
CARM

query

>add-table newtable0:ssn,phone,name,depname,depaddress 

create table newtable0 as 
            select ssn,phone,name,depname,depaddress 
            from SKEYssn_name
            left join ssn_depname_1_1 on ssn_depname_1_1.sidssn_name=SKEYssn_name.sidssn_name
            left join SKEYssn_depaddress on SKEYssn_depaddress.sidssn_depaddress=ssn_depname_1_1.sidssn_depaddress
            left join ssn_depaddress on ssn_depaddress.sidssn_depaddress=ssn_depname_1_1.sidssn_depaddress
            left join ssn_name on ssn_name.sidssn_name=SKEYssn_name.sidssn_name
            left join ssn_phone_1 on ssn_phone_1.sidssn_name=SKEYssn_name.sidssn_name

>create table newtable0 as select ssn,phone,name,depname,depaddress from SKEYssn_name left join ssn_depname_1_1 on ssn_depname_1_1.sidssn_name=SKEYssn_name.sidssn_name left join SKEYssn_depaddress on SKEYssn_depaddress.sidssn_depaddress=ssn_depname_1_1.sidssn_depaddress left join ssn_depaddress on ssn_depaddress.sidssn_depaddress=ssn_depname_1_1.sidssn_depaddress left join ssn_name on ssn_name.sidssn_name=SKEYssn_name.sidssn_name left join ssn_phone_1 on ssn_phone_1.sidssn_name=SKEYssn_name.sidssn_name


>trace-database demo2_7

</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo2/demo2_7_tracedb.xml)

select * from newtable0
[view new table0 JSON](/data/demo2/demo2_7_tracedb.xml/newtable0)
---

#### mappings

---
#### reverse mappings
<pre>
</pre>