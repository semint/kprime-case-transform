# Story demo2_6

## Given

<pre>
table1: SSN , Phone 
table3: SSN , Name 
table7: SSN , DepName 
table8: DepName , DepAddress 

pkey_table1  PRIMARY_KEY  SSN , Phone  ->  SSN , Phone 
pkey_table3  PRIMARY_KEY  SSN  ->  SSN 
table1_table3.DOUBLE_INCLUSION7  DOUBLE_INCLUSION  SSN  ->  SSN 
pkey_table7  PRIMARY_KEY  SSN  ->  SSN 
pkey_table8  PRIMARY_KEY  DepName  ->  DepName 
table7_table3.INCLUSION12  INCLUSION  SSN  ->  SSN 
table7_table1.INCLUSION13  INCLUSION  SSN  ->  SSN 
table7_table8.DOUBLE_INCLUSION14  DOUBLE_INCLUSION  DepName  ->  DepName 
</pre>
---
## When

#### changeset
<pre>
>set-source demo2
>set-db-name demo2_6

>changeset-clean
>add-cs-oids ssn_name
>changeset-apply
>changeset-exec-sql

>changeset-clean
>add-cs-oids ssn_depaddress
>changeset-apply
>changeset-exec-sql

>changeset-clean

>del-cs-table ssn_phone
>del-cs-mapping ssn_phone

>del-cs-table ssn_depname_1
>del-cs-mapping ssn_depname_1

>del-cs-table ssn_depname
>del-cs-mapping ssn_depname

>changeset-apply
>changeset-exec-mappings
>trace-database demo2_6
</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo2/demo2_6_tracedb.xml)

---

#### mappings

---
#### reverse mappings
<pre>
</pre>