# Story demo2_2

## Given

[DEMO2_1](Story_demo2_1.md#2)

---
## When demo 2.2

### vertical decomposition MVD SSN-->Name
#### trigger MVD.1:
<pre>
LHS=SSN, 
RHS=Name, 
REST=DepName,DepAdd, 
CONSTR=FD2,DOUBLE_INC 
RHS'=_
</pre>

---
#### changeset

<pre>
>set-source demo2
>set-db-name demo2_2

>changeset-clean

>add-cs-table ssn_name:SSN,Name
>add-cs-table ssn_dep:SSN,DepName,DepAddress
>add-cs-key ssn_phone:SSN,Phone
>add-cs-key ssn_name:SSN
>add-cs-key ssn_dep:SSN
>add-cs-double-inc ssn_phone:SSN<->ssn_dep:SSN
>add-cs-double-inc ssn_phone:SSN<->ssn_name:SSN
>add-cs-double-inc ssn_name:SSN<->ssn_dep:SSN
>add-cs-double-inc ssn_dep:DepName<->ssn_dep:DepAddress

>del-cs-table ssn_others
del-cs-mapping ssn_others

add-cs-mapping ssn_name select SSN,Name from table0
add-cs-mapping ssn_dep select SSN,DepName,DepAddress from table0

>changeset-apply
changeset-exec-mappings
>trace-database demo2_2
</pre>
---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo2/demo2_2_tracedb.xml)

>select * from ssn_name

[view ssn_name JSON](/data/demo2/demo2_2_tracedb.xml/ssn_name)

>select * from ssn_dep

[view ssn_dep JSON](/data/demo2/demo2_2_tracedb.xml/ssn_dep)

---
#### mappings
<pre>
ssn_phone : select SSN,Phone from table0
ssn_name : select SSN,Name from table0
ssn_dep : select SSN,DepName,DepAdd from table0
</pre>

#### reverse mappings
<pre>
table0 : select * from ssn_dep
   join ssn_phone on ssn_dep.SSN=ssn_phone.SSN
   join ssn_name on ssn_dep.SSN=ssn_name.SSN
</pre>