# Story demo2_3

## Given

<pre>
ssn_phone: SSN,Phone
ssn_name: SSN,Name
ssn_dep: SSN,DepName,DepAddress

PK[ssn_phone]:SSN,Phone
PK[ssn_name]:SSN
PK[ssn_dep]:SSN

DOUBLE_INC[ssn_dep,ssn_dep]:DepName,DepAddress
DOUBLE_INC2[ssn_phone,ssn_dep]:SSN,SSN
DOUBLE_INC3[ssn_name,ssn_dep]:SSN,SSN
DOUBLE_INC4[ssn_phone,ssn_name]:SSN,SSN
</pre>
---
## When

### horizontal decomposition for nullables of ssn_dep:DepName,DepAddress
#### trigger HD.1:
<pre>
trigger HD1:
LHS=,
RHS=,
REST=,
CONSTR=
RHS'=_
</pre>

---
#### changeset
<pre>
>set-source demo2
>set-db-name demo2_3

>changeset-clean
tables:
>add-cs-table ssn_without_dep:SSN
>add-cs-table ssn_with_dep:SSN,DepName,DepAddress

constraints:
>add-cs-key ssn_without_dep:SSN
>add-cs-key ssn_with_dep:SSN
>add-cs-inclusion ssn_without_dep:SSN-->ssn_name:SSN
>add-cs-foreign-key ssn_without_dep:SSN-->ssn_phone:SSN
>add-cs-inclusion ssn_with_dep:SSN-->ssn_name:SSN
>add-cs-foreign-key ssn_with_dep:SSN-->ssn_phone:SSN
>add-cs-functional ssn_with_dep:DepName-->ssn_with_dep:DepAddress
>add-cs-functional ssn_with_dep:DepAddress-->ssn_with_dep:DepName

delete ssn_dep:
>del-cs-table ssn_dep
del-cs-mapping ssn_dep

add-cs-mapping ssn_without_dep select SSN from table0 where DepName is null and DepAddress is null
add-cs-mapping ssn_with_dep select SSN,DepName,DepAddress from table0 where DepName is not null and DepAddress is not null

>changeset-apply
changeset-exec-mappings
>trace-database demo2_3

</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo2/demo2_3_tracedb.xml)

>select * from ssn_without_dep

[view ssn_without_dep JSON](/data/demo2/demo2_3_tracedb.xml/ssn_without_dep)

>select * from ssn_with_dep

[view ssn_with_dep JSON](/data/demo2/demo2_3_tracedb.xml/ssn_with_dep)

---

#### mappings
ssn_phone : select SSN,Phone from table0

ssn_name : select SSN,Name from table0

ssn_without_dep : select SSN from table0 where DepName is null,DepAdd is null

ssn_with_dep : select SSN,DepName,DepAdd from table0 where DepName is not null,DepAdd is not null

---
#### reverse mappings
<pre>
table0 : select * from ssn_without_dep
           join ssn_phone on ssn_without_dep.SSN=ssn_phone.SSN
           join ssn_name on ssn_without_dep.SSN=ssn_name.SSN
            union
        select * from ssn_with_dep
           join ssn_phone on ssn_with_dep.SSN=ssn_phone.SSN
           join ssn_name on ssn_with_dep.SSN=ssn_name.SSN
</pre>