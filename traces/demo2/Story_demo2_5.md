# Story demo2_5

## Given

<pre>
ssn_phone: SSN,Phone
ssn_name: SSN,Name
ssn_with_dep: SSN,DepName,DepAddress

PK[ssn_phone]:SSN,Phone
PK[ssn_name]:SSN
PK[ssn_with_dep]:SSN

</pre>
---
## When

### vertical decomposition for ssn_with_dep:SSN,DepName,DepAddress
#### trigger HD.1:
<pre>
trigger HD1:
LHS=,
RHS=,
REST=,
CONSTR=
RHS'=_
</pre>

---
#### changeset
<pre>
>set-source demo2
>set-db-name demo2_5

>changeset-clean
tables:
>add-cs-table ssn_depname:SSN,DepName
>add-cs-table ssn_depaddress:DepName,DepAddress

constraints:
>add-cs-key ssn_depname:SSN,DepName
>add-cs-key ssn_depaddress:DepName
>add-cs-inclusion ssn_depname:SSN-->ssn_name:SSN
>add-cs-inclusion ssn_depname:SSN-->ssn_phone:SSN
>add-cs-double-inc ssn_depname:DepName<->ssn_depaddress:DepName

delete ssn_with_dep:
>del-cs-table ssn_with_dep
del-cs-mapping ssn_with_dep

add-cs-mapping ssn_depname select SSN,DepName from table0 where DepName is not null and DepAddress is not null
add-cs-mapping ssn_depaddress select distinct DepName,DepAddress from table0 where DepName is not null and DepAddress is not null

>changeset-apply
changeset-exec-mappings
>trace-database demo2_5

</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo2/demo2_5_tracedb.xml)

>select * from ssn_depname

[view ssn_depname JSON](/data/demo2/demo2_5_tracedb.xml/ssn_depname)

>select * from ssn_depaddress

[view ssn_depaddress JSON](/data/demo2/demo2_5_tracedb.xml/ssn_depaddress)

---

#### mappings

---
#### reverse mappings
<pre>

</pre>