# Story demo2-meta

## setup meta table0

>database-clean
>changeset-clean
>set-source demo2
>set-db-name demo2_0

tables:
>add-table table0:SSN,Phone,Name,DepName,DepAddress

constraints:
>add-multivalued table0:SSN-->Phone

>add-functional table0:SSN-->Name

>add-functional table0:SSN-->DepName

>add-double-inc table0:DepName<->table0:DepAddress

>all