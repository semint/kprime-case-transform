# Story demo2


---

## Start: Relational Denormalized
<img src="http://semint.it/kprime/img/demo1/demo1-denormalizzato.png" width=70%>
---
## Target: Relational Canonical
<img src="http://semint.it/kprime/img/demo1/demo1-7-carm.png" width=70%>

---
## Target ORM view
<img src="http://semint.it/kprime/img/demo1/demo1-orm.png" width=70%>

---
## Decomposition Steps

*given*

a database and constraints, mappings (as views)

*when*

condition trigger transform on tables and constraints

*then*

use a selector/extractor pattern to

create and apply a changeset with
   1. phisical tables on secondary database
   2. logical tables and constraints
   3. create mappings/views sqlrewrite

repeat from *when* until
final database and mappings

---
## Transformations

Given

[Denormalized Table SQL](Story_demo1-sql.md#2)

[Denormalized Table Constraints](Story_demo1-meta.md#2)


Here are the Reverse Engineering steps:

1. [vertical decomposition MVD SSN->>Phone](Story_demo1_1.md)
2. [vertical decomposition FD SSN->Name](Story_demo1_2.md)
3. [horizontal decomposition NULL on Department](Story_demo1_3.md)
4. [ignore redoundant table (not-employee)](Story_demo1_4.md)
5. [vertical decomposition FD SSN->Department](Story_demo1_5.md)
6. [generate OID for each entity](Story_demo1_6_oids.md)
7. [Canonical ARM](Story_demo1_7_newtable0.md)

[CARM insert](Story_demo1_8a_insert.md#2)
[CARM delete](Story_demo1_8b_delete.md#2)
[CARM diff](Story_demo1_9_diff.md#2)
