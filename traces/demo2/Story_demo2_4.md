# Story demo2_4

## Given

<pre>
table1: SSN,Phone
table3: SSN,Name
table5: SSN
table6: SSN,DepName,DepAddress

PK[table1]:SSN,Phone
PK[table3]:SSN
PK[table5]:SSN
PK[table6]:SSN

</pre>
---
## When

### delete table5
#### changeset
<pre>
>set-source demo2
>set-db-name demo2_4

>changeset-clean

>del-cs-table ssn_without_dep
del-cs-mapping ssn_without_dep

>changeset-apply
changeset-exec-mappings
>trace-database demo2_4

</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo2/demo2_4_tracedb.xml)

---

#### mappings
ssn_phone : select SSN,Phone from table0

ssn_name : select SSN,Name from table0

ssn_with_dep : select SSN,DepName,DepAdd from table0 where DepName is not null,DepAdd is not null

---
#### reverse mappings
<pre>
</pre>