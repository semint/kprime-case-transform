# Story demo1_1

## Given

[Table0 SQL](Story_demo1-sql.md#2)

[Table0 META](Story_demo1-meta.md#2)
---
## When demo 1.1

### vertical decomposition MVD SSN->>Phone
#### trigger MVD.1:
<pre>
LHS=SSN, 
RHS=Phone, 
REST=Name,DepName,DepAdd, 
CONSTR=FD1,FD2,DOUBLE_INC 
RHS'=_
</pre>

---
#### changeset

>set-source demo1

>set-db-name demo1_1

>changeset-clean

>add-cs-table table1:SSN,Phone

>add-cs-table table2:SSN,Name,DepName,DepAddress

>add-cs-double-inc table1:SSN<->table2:SSN

>del-cs-table table0

>add-cs-mapping table1 select SSN,Phone from table0

>add-cs-mapping table2 select SSN,Name,DepName,DepAddress from table0

>changeset-apply

>changeset-exec-mappings

>trace-database demo1_1

---
## Then

#### database

>all

[view DIAGRAM](/chart/classes/demo1/demo1_1_tracedb.xml)

>select * from table1

[view table1 JSON](/data/demo1/demo1_1_tracedb.xml/table1)

>select * from table2

[view table2 JSON](/data/demo1/demo1_1_tracedb.xml/table1)

---
#### mappings
<pre>
table1 : select SSN,Phone from table0
table2 : select SSN,Name,DepName,DepAdd from table0
</pre>

#### reverse mappings
<pre>
table0 : select * from table1 
    join table2 on table1.SSN=table2.SSN
</pre>