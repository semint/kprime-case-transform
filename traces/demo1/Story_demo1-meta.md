# Story demo1-meta

## setup meta table0

>set-source demo1
>set-db-name demo1_1
>database-clean
>changeset-clean

tables:
>add-table table0:SSN,Phone,Name,DepName,DepAddress

constraints:
>add-multivalued table0:SSN-->Phone

>add-functional table0:SSN-->Name

>add-functional table0:SSN-->DepName

>add-double-inc table0:DepName<->table0:DepAddress

>all