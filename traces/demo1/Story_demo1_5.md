# Story demo1_5

## Given

<pre>
table1: SSN,Phone
table3: SSN,Name
table6: SSN,DepName,DepAddress

PK[table1]:SSN,Phone
PK[table3]:SSN
PK[table6]:SSN

</pre>
---
## When

### vertical decomposition for table6:SSN,DepName,DepAddress
#### trigger HD.1:
<pre>
trigger HD1:
LHS=,
RHS=,
REST=,
CONSTR=
RHS'=_
</pre>

---
#### changeset
<pre>
>set-source demo1
>set-db-name demo1_5

>changeset-clean
tables:
>add-cs-table table7:SSN,DepName
>add-cs-table table8:DepName,DepAddress

constraints:
>add-cs-key table7:SSN
>add-cs-key table8:DepName
>add-cs-inclusion table7:SSN-->table3:SSN
>add-cs-inclusion table7:SSN-->table1:SSN
>add-cs-double-inc table7:DepName<->table8:DepName

delete table4:
>del-cs-table table6
>del-cs-mapping table6

>add-cs-mapping table7 select SSN,DepName from table0 where DepName is not null and DepAddress is not null
>add-cs-mapping table8 select distinct DepName,DepAddress from table0 where DepName is not null and DepAddress is not null

>changeset-apply
>changeset-exec-mappings
>trace-database demo1_5

</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo1/demo1_5_tracedb.xml)

>select * from table7

[view table7 JSON](/data/demo1/demo1_5_tracedb.xml/table7)

>select * from table8

[view table8 JSON](/data/demo1/demo1_5_tracedb.xml/table8)

---

#### mappings

---
#### reverse mappings
<pre>

</pre>