# Story demo1_2

## Given

[DEMO1_1](Story_demo1_1.md#2)

---
## When demo 1.2

### vertical decomposition MVD SSN-->Name
#### trigger MVD.1:
<pre>
LHS=SSN, 
RHS=Name, 
REST=DepName,DepAdd, 
CONSTR=FD2,DOUBLE_INC 
RHS'=_
</pre>

---
#### changeset

<pre>
>set-source demo1
>set-db-name demo1_2

>changeset-clean

>add-cs-table table3:SSN,Name
>add-cs-table table4:SSN,DepName,DepAddress
>add-cs-key table1:SSN,Phone
>add-cs-key table3:SSN
>add-cs-key table4:SSN
>add-cs-double-inc table1:SSN<->table4:SSN
>add-cs-double-inc table1:SSN<->table3:SSN
>add-cs-double-inc table3:SSN<->table4:SSN
>add-cs-double-inc table4:DepName<->table4:DepAddress

>del-cs-table table2
>del-cs-mapping table2

>add-cs-mapping table3 select SSN,Name from table2
>add-cs-mapping table4 select SSN,DepName,DepAddress from table2

>changeset-apply
>changeset-exec-mappings
>trace-database demo1_2
</pre>
---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo1/demo1_2_tracedb.xml)

>select * from table3

[view table3 JSON](/data/demo1/demo1_2_tracedb.xml/table3)

>select * from table4

[view table4 JSON](/data/demo1/demo1_2_tracedb.xml/table4)

---
#### mappings
<pre>
table1 : select SSN,Phone from table0
table3 : select SSN,Name from table0
table4 : select SSN,DepName,DepAdd from table0
</pre>

#### reverse mappings
<pre>
table0 : select * from table4
   join table1 on table4.SSN=table1.SSN
   join table3 on table4.SSN=table3.SSN
</pre>