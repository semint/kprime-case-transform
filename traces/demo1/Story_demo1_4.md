# Story demo1_4

## Given

<pre>
table1: SSN,Phone
table3: SSN,Name
table5: SSN
table6: SSN,DepName,DepAddress

PK[table1]:SSN,Phone
PK[table3]:SSN
PK[table5]:SSN
PK[table6]:SSN

</pre>
---
## When

### delete table5
#### changeset
<pre>
>set-source demo1
>set-db-name demo1_4

>changeset-clean

>del-cs-table table5
>del-cs-mapping table5

>changeset-apply
>changeset-exec-mappings
>trace-database demo1_4

</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo1/demo1_4_tracedb.xml)

---

#### mappings
table1 : select SSN,Phone from table0

table3 : select SSN,Name from table0

table6 : select SSN,DepName,DepAdd from table0 where DepName is not null,DepAdd is not null

---
#### reverse mappings
<pre>
</pre>