# Story demo1_6

## Given

<pre>
table1: SSN , Phone 
table3: SSN , Name 
table7: SSN , DepName 
table8: DepName , DepAddress 

pkey_table1  PRIMARY_KEY  SSN , Phone  ->  SSN , Phone 
pkey_table3  PRIMARY_KEY  SSN  ->  SSN 
table1_table3.DOUBLE_INCLUSION7  DOUBLE_INCLUSION  SSN  ->  SSN 
pkey_table7  PRIMARY_KEY  SSN  ->  SSN 
pkey_table8  PRIMARY_KEY  DepName  ->  DepName 
table7_table3.INCLUSION12  INCLUSION  SSN  ->  SSN 
table7_table1.INCLUSION13  INCLUSION  SSN  ->  SSN 
table7_table8.DOUBLE_INCLUSION14  DOUBLE_INCLUSION  DepName  ->  DepName 
</pre>
---
## When

#### changeset
<pre>
>set-source demo1
>set-db-name demo1_6

>changeset-clean
>add-cs-oids table3
>changeset-exec-sql

>changeset-clean
>add-cs-oids table8
>changeset-exec-sql

>changeset-apply
>changeset-exec-mappings
>trace-database demo1_6
</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo1/demo1_6_tracedb.xml)

---

#### mappings

---
#### reverse mappings
<pre>
</pre>