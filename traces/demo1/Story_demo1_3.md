# Story demo1_3

## Given

<pre>
table1: SSN,Phone
table3: SSN,Name
table4: SSN,DepName,DepAddress

PK[table1]:SSN,Phone
PK[table3]:SSN
PK[table4]:SSN

DOUBLE_INC[table4,table4]:DepName,DepAddress
DOUBLE_INC2[table1,table4]:SSN,SSN
DOUBLE_INC3[table3,table4]:SSN,SSN
DOUBLE_INC4[table1,table3]:SSN,SSN
</pre>
---
## When

### horizontal decomposition for nullables of table4:DepName,DepAddress
#### trigger HD.1:
<pre>
trigger HD1:
LHS=,
RHS=,
REST=,
CONSTR=
RHS'=_
</pre>

---
#### changeset
<pre>
>set-source demo1
>set-db-name demo1_3

>changeset-clean
tables:
>add-cs-table table5:SSN
>add-cs-table table6:SSN,DepName,DepAddress

constraints:
>add-cs-key table5:SSN
>add-cs-key table6:SSN
>add-cs-inclusion table5:SSN-->table1:SSN
>add-cs-foreign-key table5:SSN-->table3:SSN
>add-cs-inclusion table6:SSN-->table1:SSN
>add-cs-foreign-key table6:SSN-->table3:SSN
>add-cs-functional table6:DepName-->table6:DepAddress
>add-cs-functional table6:DepAddress-->table6:DepName

delete table4:
>del-cs-table table4
>del-cs-mapping table4

>add-cs-mapping table5 select SSN from table0 where DepName is null and DepAddress is null
>add-cs-mapping table6 select SSN,DepName,DepAddress from table0 where DepName is not null and DepAddress is not null

>changeset-apply
>changeset-exec-mappings
>trace-database demo1_3

</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo1/demo1_3_tracedb.xml)

>select * from table5

[view table5 JSON](/data/demo1/demo1_3_tracedb.xml/table5)

>select * from table6

[view table6 JSON](/data/demo1/demo1_2_tracedb.xml/table6)

---

#### mappings
table1 : select SSN,Phone from table0

table3 : select SSN,Name from table0

table5 : select SSN from table0 where DepName is null,DepAdd is null

table6 : select SSN,DepName,DepAdd from table0 where DepName is not null,DepAdd is not null

---
#### reverse mappings
<pre>
table0 : select * from table5
           join table1 on table5.SSN=table1.SSN
           join table3 on table5.SSN=table3.SSN
            union
        select * from table6
           join table1 on table6.SSN=table1.SSN
           join table3 on table6.SSN=table3.SSN
</pre>