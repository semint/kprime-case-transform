# Story demo1_6

## Given

<pre>
Tables:
table1: SSN , Phone 
table3: Name , sidtable3 
table7: SSN , DepName 
table8: DepAddress , sidtable8 
SKEYtable3: sidtable3 , SSN 
table1_1: sidtable3 , Phone 
table7_1: sidtable3 , DepName 
SKEYtable8: sidtable8 , DepName 
table7_1_1: sidtable8 , sidtable3 
</pre>
---
## When

### delete table5
#### changeset
<pre>
CARM

query

>add-table newtable0:ssn,phone,name,depname,depaddress 

create table newtable0 as 
            select ssn,phone,name,depname,depaddress 
            from SKEYtable3
            left join table7_1_1 on table7_1_1.SIDTABLE3=SKEYtable3.SIDTABLE3
            left join SKEYtable8 on SKEYtable8.sidtable8=table7_1_1.sidtable8
            left join table8 on table8.sidtable8=table7_1_1.sidtable8
            left join table3 on table3.sidtable3=SKEYtable3.sidtable3
            left join table1_1 on table1_1.sidtable3=SKEYtable3.sidtable3

>create table newtable0 as select ssn,phone,name,depname,depaddress from SKEYtable3 left join table7_1_1 on table7_1_1.SIDTABLE3=SKEYtable3. SIDTABLE3 left join SKEYtable8 on SKEYtable8.sidtable8=table7_1_1.sidtable8 left join table8 on table8.sidtable8=table7_1_1.sidtable8 left join table3 on table3.sidtable3=SKEYtable3.sidtable3 left join table1_1 on table1_1.sidtable3=SKEYtable3.sidtable3

>trace-database demo1_7

</pre>

---
## Then
#### database

>all

[view DIAGRAM](/chart/classes/demo1/demo1_7_tracedb.xml)

select * from newtable0
[view new table0 JSON](/data/demo1/demo1_7_tracedb.xml/newtable0)
[view new table0 JSON](/data/demo1/demo1_7_tracedb.xml/table7_1_1)
---

#### mappings

---
#### reverse mappings
<pre>
</pre>