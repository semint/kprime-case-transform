# Story patterns of db decomposition
To better identify the potential entities in a schema, it is good practice to render
the schema in a suitable decomposed state. This chapter provides a taxonomy
of abstract patterns for database schema decompositions. 
We consider both [vertical](/slide/vertical/Story_story_vertical1.md) and [horizontal](/slide/horizontal/Story_story_horizontal.md) decompositions, showing example instances that illustrate 
the pattern matching mechanism, and finally, determining the placement
for object identifiers given the decomposition scenario.
