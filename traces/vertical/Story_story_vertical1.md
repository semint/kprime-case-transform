# Story story_vertical1

## Vertical Decompose 
### Functional Dependency
To learn about ([Functional Dependency](https://en.wikipedia.org/wiki/Functional_dependency) 1).

add-trace vertical 

current-trace vertical

trace-goal vertical1

add-table person:ssn,deptname,deptadd,age

add-functional person:deptname-->deptadd

set-db-name vertical1

save-database

---
trans-applicability 

<pre>
vertical COMPOSE Empty rules.
 DECOMPOSE decomposeApplicable true 
 originTable [person]
 all [ssn, deptname, deptadd, age]
 keys []
 lhss [deptname]
 rhss [deptadd]
 rests [ssn, age]
 table [person]
 view1 [deptname, ssn, age]
 view2 [deptname, deptadd]

horizontal COMPOSE Empty rules.
 DECOMPOSE required [originTable, condition] parameter(s).
</pre>
---
trans-apply vertical decompose

current-database vertical revision_20200915_165043_0378735881_db.xml 

<pre>
Tables:
person_1: deptname , ssn , age 
person_2: deptname , deptadd 

-------------
Mappings:
 person_1  
 person_2  

-------------
Constraints:
 person.doubleInclusion  DOUBLE_INCLUSION  deptname  ->  deptname 
 person_2.primaryKey  PRIMARY_KEY  deptname  ->   
</pre>

---
## Vertical Compose 
### Functional Dependency 1

current-database vertical revision_20200915_174843_0600033560_db.xml

trans-applicability

<pre>
vertical COMPOSE composeApplicable true 
 originTable1 [person_1]
 originTable2 [person_2]
 lhs [deptname, ssn, age]
 rhs [deptname, deptadd]
 originTable [person]
</pre>
---
trans-apply vertical compose

current-database vertical 20200915_204253_0078669612_db.xml

<pre>
Tables:
person: deptname , ssn , age , deptname , deptadd 

-------------
Mappings:
 person_1  
 person_2  

-------------
Constraints:
</pre>